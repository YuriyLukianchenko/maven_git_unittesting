package com.lukianchenko;

import com.lukianchenko.error.employee.EmployeeDuplicationException;
import com.lukianchenko.error.employee.UnExistingEmployeeException;
import com.lukianchenko.service.EmployeeService;
import com.lukianchenko.service.EmployeeServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


public class EmployeeServiceImplTest {

    private EmployeeService employeeService = new EmployeeServiceImpl();

    @BeforeEach
    void refreshStaticMap() {
        employeeService.deleteAllEmployees();
    }

    @Test
    void create_whenEmployeesAreAdded_thenEmployeeServiceContainsEmployees() {
        createDefaultEmployees();
        assertTrue(Optional.of(employeeService.get("1")).isPresent());
        assertTrue(Optional.of(employeeService.get("2")).isPresent());
    }

    @Test
    void update_whenEmployeeIsUpdated_thenEmployeeServiceContainsUpdattedInfoAboutEmployee() {
        createDefaultEmployees();
        employeeService.update("1", "Nick");
        assertEquals("Nick", employeeService.get("1").getName());
    }

    @Test
    void get_whenEmployeeWithUnexistedIdIsGotten_thenThrowUnexistingEmployeeException() {
        createDefaultEmployees();
        assertThrows(UnExistingEmployeeException.class, () -> employeeService.get("5"));
    }

    @Test
    void update_whenEmployeeWithUnexistedIdIsUpdated_thenThrowUnexistingEmployeeException() {
        createDefaultEmployees();
        assertThrows(UnExistingEmployeeException.class, () -> employeeService.update("5","Bobi"));
    }

    @Test
    void create_whenEmployeeWithExistedIdIsCreated_thenThrowEmployeeDuplicationException() {
        createDefaultEmployees();
        assertThrows(EmployeeDuplicationException.class, () -> employeeService.create("2","Bob"));

    }

    private void createDefaultEmployees() {

        employeeService.create("1", "John");
        employeeService.create("2", "Bob");
    }

}
