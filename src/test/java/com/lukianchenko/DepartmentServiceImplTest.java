package com.lukianchenko;

import com.lukianchenko.domain.Employee;
import com.lukianchenko.error.department.DepartmentDuplicationException;
import com.lukianchenko.error.department.UnExistingDepartmentException;
import com.lukianchenko.service.DepartmentService;
import com.lukianchenko.service.DepartmentServiceImpl;
import com.lukianchenko.service.EmployeeService;
import com.lukianchenko.service.EmployeeServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class DepartmentServiceImplTest {
    private EmployeeService employeeService = new EmployeeServiceImpl();
    private DepartmentService departmentService = new DepartmentServiceImpl(employeeService);

    @BeforeEach
    void refreshStaticMap() {
        employeeService.deleteAllEmployees();
        departmentService.deleteAllDepartments();
    }

    @Test
    void create_whenDepartmentsAreCreated_thenDepartmentServiceContainsDepartments() {
        createDefaultDepartments();
        assertTrue(Optional.of(departmentService.get("1")).isPresent());
        assertTrue(Optional.of(departmentService.get("2")).isPresent());
    }

    @Test
    void update_whenDepartmentIsUpdated_thenDepartmentServiceContainsUpdatedInfo() {
        createDefaultDepartments();
        departmentService.update("2","Security");
        assertEquals("Security", departmentService.get("2").getName());
    }

    @Test
    void get_whenDepartmentWithUnExistedIdIsGotten_thenThrowUnExistingDepartmentException() {
        createDefaultDepartments();
        assertThrows(UnExistingDepartmentException.class, () -> departmentService.get("7"));
    }

    @Test
    void update_whenDepartmentWithUnExistedIdIsUpdated_thenThrowUnExistingDepartmentException() {
        createDefaultDepartments();
        assertThrows(UnExistingDepartmentException.class, () -> departmentService.update("7", "Security"));
    }

    @Test
    void create_whenDepartmentWithExistedIdIsCreated_thenThrowDepartmentDuplicationException() {
        createDefaultDepartments();
        assertThrows(DepartmentDuplicationException.class, () -> departmentService.create("1","Security"));
    }

    @Test
    void assignToDepartment_whenEmployeeIsAssignedToDepartment_thenEmployeesDepartmentFieldContainsAssignedDepartment() {
        createDefaultDepartments();
        employeeService.create("1","John");
        departmentService.assignDepartmentToEmployee("1","2");
        Employee employee = employeeService.get("1");
        assertEquals("2", employee.getDepartmentId());
    }

    @Test
    void assignToDepartment_whenEmployeeIsAssignedToUnExistedDepartment_thenThrowUnExistingDepartmentException() {
        createDefaultDepartments();
        employeeService.create("1","John");
        assertThrows(UnExistingDepartmentException.class, () -> departmentService.assignDepartmentToEmployee("1","3"));
    }

    @Test
    void getAllEmployeesOfDepartments_whenEmployeesOfDepartmentAreGotten_thenListWithTheseEmployeesIsReturned() {
        createDefaultDepartments();
        createDefaultEmployees();
        departmentService.assignDepartmentToEmployee("1","2");
        departmentService.assignDepartmentToEmployee("2","2");
        List<Employee> list = departmentService.getAllEmployeesByDepartment("2");
        assertEquals(2, list.size());
        assertEquals(list.get(0),employeeService.get("1"));
        assertEquals(list.get(1),employeeService.get("2"));
    }

    private void createDefaultEmployees() {
        employeeService.create("1","John");
        employeeService.create("2","Bob");
    }

    private void createDefaultDepartments() {
        departmentService.create("1", "Legal");
        departmentService.create("2", "Delivery");
    }
}
