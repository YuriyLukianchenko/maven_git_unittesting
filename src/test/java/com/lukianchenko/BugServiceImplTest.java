package com.lukianchenko;

import com.lukianchenko.error.bug.BugDuplicationException;
import com.lukianchenko.error.bug.UnExistingBugException;
import com.lukianchenko.service.BugService;
import com.lukianchenko.service.BugServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BugServiceImplTest {

    private BugService bugService = new BugServiceImpl();

    @BeforeEach
    void refreshStaticMap() {
        bugService.deleteAllBugs();
    }
    @Test
    void create_whenBugIsCreated_thenBugServiceContainsThisBug() {
        createDefaultBugs();
        assertEquals("1",bugService.get("1").getId());
    }

    @Test
    void update_whenBugIsUpdated_thenBugContainsNewInfo() {
        createDefaultBugs();
        bugService.update("2", "newBug");
        assertEquals("newBug", bugService.get("2").getDescription());
    }

    @Test
    void assignToUser_whenBugIsAssignedToUser_thenBugContainsUserId() {
        createDefaultBugs();
        bugService.assignToUser("2", "Ivan49");
        assertEquals("Ivan49", bugService.get("2").getUserId());
    }

    @Test
    void create_whenCreateBugWithExistingId_thenThrowBugDuplicationException() {
        createDefaultBugs();
        assertThrows(BugDuplicationException.class, ()-> bugService.create("1", "duplicationBug"));
    }

    @Test
    void update_whenUpdateBugWithUnExistedId_thenThrowUnExistingBugException() {
        createDefaultBugs();
        assertThrows(UnExistingBugException.class,() -> bugService.update("3", "unexistedBug"));
    }

    @Test
    void assignToUser_whenBugWithUnexistedIdIsAssignedToUser_thenThrowUnexistingBugException() {
        createDefaultBugs();
        assertThrows(UnExistingBugException.class,() -> bugService.assignToUser("22", "userAlpha"));
    }

    private void createDefaultBugs() {
        bugService.create("1", "duplicationBug");
        bugService.create("2", "emptyBug");

    }
}
