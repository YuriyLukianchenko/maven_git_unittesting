package com.lukianchenko.domain;

import lombok.Data;

@Data
public class Employee {
    private String id;
    private String name;
    private String departmentId;

    public Employee(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Employee() {
    }
}
