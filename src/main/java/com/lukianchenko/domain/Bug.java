package com.lukianchenko.domain;

import lombok.Data;

@Data
public class Bug {
    private String id;
    private String description;
    private String userId;

    public Bug(String id, String description) {
        this.id = id;
        this.description = description;
    }

    public Bug() {
    }
}
