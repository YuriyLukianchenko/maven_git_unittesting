package com.lukianchenko.error.employee;

public class UnExistingEmployeeException extends RuntimeException {
    public UnExistingEmployeeException() {
    }
    public UnExistingEmployeeException(String message) {
        super(message);
    }
}
