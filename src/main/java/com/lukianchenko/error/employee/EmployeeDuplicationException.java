package com.lukianchenko.error.employee;

public class EmployeeDuplicationException extends RuntimeException {
    public EmployeeDuplicationException(String message) {
        super(message);
    }
    public EmployeeDuplicationException() {
    }
}
