package com.lukianchenko.error.bug;

public class UnExistingBugException extends RuntimeException {
    public UnExistingBugException(String msg) {
        super(msg);
    }
}
