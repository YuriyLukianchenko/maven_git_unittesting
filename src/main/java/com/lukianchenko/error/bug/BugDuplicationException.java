package com.lukianchenko.error.bug;

public class BugDuplicationException extends RuntimeException {
    public BugDuplicationException(String msg) {
        super(msg);
    }

    public BugDuplicationException() {
    }
}
