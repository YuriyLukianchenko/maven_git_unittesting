package com.lukianchenko.error.department;

public class DepartmentDuplicationException extends RuntimeException {
    public DepartmentDuplicationException(String message) {
        super(message);
    }
    public DepartmentDuplicationException() {
    }
}
