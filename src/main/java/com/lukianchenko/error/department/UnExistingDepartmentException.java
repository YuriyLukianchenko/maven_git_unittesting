package com.lukianchenko.error.department;

public class UnExistingDepartmentException extends RuntimeException  {
    public UnExistingDepartmentException(String message) {
        super(message);
    }
    public UnExistingDepartmentException() {
    }
}
