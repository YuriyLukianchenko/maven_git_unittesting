package com.lukianchenko.service;

import com.lukianchenko.domain.Bug;
import com.lukianchenko.error.bug.BugDuplicationException;
import com.lukianchenko.error.bug.UnExistingBugException;

import java.util.HashMap;
import java.util.Map;

public class BugServiceImpl implements BugService{
    private static Map<String, Bug> bugs = new HashMap<>();

    @Override
    public void create(String id, String description) {
        Bug bug = new Bug(id, description);
        if (bugs.containsKey(id)) {
            throw new BugDuplicationException(String.format("Bug with id = %s already exist", id));
        } else {
            bugs.put(id, bug);
        }
    }

    @Override
    public void update(String id, String description) {
        if(bugs.containsKey(id)) {
            bugs.get(id).setDescription(description);
        } else {
            throw new UnExistingBugException(String.format("Bug with id = %s does not exist", id));
        }
    }

    @Override
    public void assignToUser(String bugId, String userId) {
        if(bugs.containsKey(bugId)) {
            bugs.get(bugId).setUserId(userId);
        } else {
            throw new UnExistingBugException(String.format("Bug with id = %s does not exist", bugId));
        }
    }

    @Override
    public Bug get(String id) {
        if(bugs.containsKey(id)) {
            return bugs.get(id);
        } else {
            throw new UnExistingBugException(String.format("Bug with id = %s does not exist", id));
        }
    }

    @Override
    public void deleteAllBugs() {
        bugs.clear();
    }
}
