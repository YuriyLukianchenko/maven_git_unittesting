package com.lukianchenko.service;

import com.lukianchenko.domain.Employee;
import com.lukianchenko.error.employee.EmployeeDuplicationException;
import com.lukianchenko.error.employee.UnExistingEmployeeException;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
public class EmployeeServiceImpl implements EmployeeService{
    private static Map<String, Employee> employees = new HashMap<>();

    @Override
    public void create(String id, String name) {
        Employee employee = new Employee(id, name);
        if (employees.containsKey(id)) {
            throw new EmployeeDuplicationException(String.format("Employee with id = %s already exists", id));
        } else {
            employees.put(id, employee);
        }
    }

    @Override
    public void update(String id, String name) {
        if (employees.containsKey(id)) {
            employees.get(id).setName(name);
        } else {
            throw new UnExistingEmployeeException(String.format("Can not update employee, employee with id = %s does not exist", id));
        }
    }

    @Override
    public Employee get(String id) {
        if (employees.containsKey(id)) {
            return employees.get(id);
        } else {
            throw new UnExistingEmployeeException(String.format("Can not get employee, employee with id = %s does not exist", id));
        }
    }

    @Override
    public List<Employee> getAllEmployees() {
        List employeesList = new ArrayList();
        employees.forEach((id, employee) -> employeesList.add(employee));
        return employeesList;
    }

    @Override
    public void deleteAllEmployees() {
        employees.clear();
    }

}
