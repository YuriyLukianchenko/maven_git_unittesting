package com.lukianchenko.service;

import com.lukianchenko.domain.Department;
import com.lukianchenko.domain.Employee;

import java.util.List;

/**
 * Service which supply methods for working with departments.
 *@author Yura
 */
public interface DepartmentService {

    /**
     * Describe new department and set it in repository.
     *
     * @param id          of the department, throws DepartmentDuplicationException
     *                    if department with same id already exists.
     * @param name        of the department.
     */
    void create(String id, String name);

    /**
     * Update bug information.
     * @param id          of the department, throws UnExistingDepartmentException
     *                    if department does not exist.
     * @param name        what will be set instead of old name.
     */
    void update(String id, String name);

    /**
     * Assign employeeId to department.
     *
     * @param employeeId  throws UnExistingDepartmentException
     *               if department does not exist.
     * @param departmentId will be assigned to employee.
     */
    void assignDepartmentToEmployee(String employeeId, String departmentId);

    /**
     * Return all employees from repository.
     * @param departmentId from which all employees will be returned.
     * @return list of employees of the department.
     */
    List<Employee> getAllEmployeesByDepartment(String departmentId);

    /**
     * Returns department by its id.
     *
     * @param id of the department.
     * @return department by id.
     */
    Department get(String id);

    /**
     * Delete all departments in repository.
     */
    void deleteAllDepartments();
}
