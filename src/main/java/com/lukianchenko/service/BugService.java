package com.lukianchenko.service;

import com.lukianchenko.domain.Bug;

/**
 * Service which supply methods for working with bugs.
 * @author Yura
 */
public interface BugService {

    /**
     * Describe new bug and set it in repository.
     *
     * @param id          of the bug, throws BugDuplicationException
     *                    if bug with same id already exists.
     * @param description contains information about bug.
     */
    void create(String id, String description);

    /**
     * Update bug information.
     * @param id          of the bug, throws UnExistingBugException
     *                    if bug does not exist.
     * @param description contains information about bug.
     */
    void update(String id, String description);

    /**
     * Assign userId to bug.
     *
     * @param bugId  throws UnExistingBugException
     *               if bug does not exist.
     * @param userId will be assigned to bug.
     */
    void assignToUser(String bugId, String userId);

    /**
     * Returns bug from repository.
     *
     * @param id is used to take bug from repository,
     *           throws UnExistingBugException if bug does not exist.
     * @return   bug from repository.
     */
    Bug get(String id);

    /**
     * Delete all bugs in repository.
     */
    void deleteAllBugs();

}
