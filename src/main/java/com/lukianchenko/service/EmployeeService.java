package com.lukianchenko.service;

import com.lukianchenko.domain.Employee;

import java.util.List;

/**
 * Service which supply methods for working with employees.
 * @author Yura
 */
public interface EmployeeService {

    /**
     * Describe new employee and set it in repository.
     *
     * @param id          of the employee, throws EmployeeDuplicationException
     *                    if employee with same id already exists.
     * @param name        of the employee.
     */
    void create(String id, String name);

    /**
     * Update employee information.
     * @param id          of the employee, throws UnExistingEmployeeException
     *                    if employee does not exist.
     * @param name        what will be set instead of old name.
     */
    void update(String id, String name);

    /**
     * Returns employee by its id.
     *
     * @param id of the employee.
     * @return employee by id.
     */
    Employee get(String id);

    /**
     * Returns all employee from repository.
     * @return list of employees.
     */
    List<Employee> getAllEmployees();

    /**
     * Delete all employees in repository.
     */
    void deleteAllEmployees();
}
