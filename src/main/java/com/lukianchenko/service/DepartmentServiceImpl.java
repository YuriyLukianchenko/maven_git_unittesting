package com.lukianchenko.service;

import com.lukianchenko.domain.Department;
import com.lukianchenko.domain.Employee;
import com.lukianchenko.error.department.DepartmentDuplicationException;
import com.lukianchenko.error.department.UnExistingDepartmentException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class DepartmentServiceImpl implements DepartmentService{

    private static Map<String, Department> departments = new HashMap<>();
    private EmployeeService employeeService;


    public DepartmentServiceImpl(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @Override
    public void create(String id, String name) {
        Department department = new Department(id, name);
        if(departments.containsKey(id)) {
            throw new DepartmentDuplicationException(String.format("Department with id = %s already exists", id));
        } else {
            departments.put(id,department);
        }
    }

    @Override
    public void update(String id, String name) {
        if (departments.containsKey(id)) {
            departments.get(id).setName(name);
        } else {
            throw new UnExistingDepartmentException(String.format("Can not update department, department with id = %s does not exist", id));
        }
    }

    @Override
    public void assignDepartmentToEmployee(String employeeId, String departmentId) {
        if(departments.containsKey(departmentId)) {
            employeeService.get(employeeId).setDepartmentId(departmentId);
        } else {
            throw new UnExistingDepartmentException(String.format("Department with id = %s does not exist", employeeId));
        }
    }

    @Override
    public List<Employee> getAllEmployeesByDepartment(String departmentId) {
        if(departments.containsKey(departmentId)) {
            return employeeService.getAllEmployees().stream()
                    .filter(employee -> employee.getDepartmentId().equals(departmentId))
                    .collect(Collectors.toList());
        } else {
            throw new UnExistingDepartmentException(String.format("Department with id = %s does not exist", departmentId));
        }
    }

    @Override
    public Department get(String id) {
        if (departments.containsKey(id)) {
            return departments.get(id);
        } else {
            throw new UnExistingDepartmentException(String.format("Can not get department, department with id = %s does not exist", id));
        }
    }

    @Override
    public void deleteAllDepartments() {
        departments.clear();
    }
}
